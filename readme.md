### 키워드 기반 취업 지원 시스템 프로젝트
#### 한국 외대 융복합 소프트웨어 전공 캡스톤 디자인 4팀
##### - 백엔드와 프론트엔드 연동 안돼있음
##### - 파이썬 장고 백엔드 사용
##### - 자연어 처리(NLP) 기반 프로젝트
- 취업을 원하는 기업에 관련된 기사를 크롤링
- 크롤링한 기사를 텍스트 마이닝을 통해 분석
- 사용한 텍스트 마이닝 기법은
  - fastText
  - TF_IDF
  - word Embedding
  - TextRank
- 형태소 분석기는 Mecab Eunjeon Project 사용
##### 1) 시스템구성 흐름도
![Flow_Chart](./img/Flow_Chart.JPG)
##### 2) 10개 기업에 대해 크롤링
![crawling](./img/crawling.JPG)
##### 3) Fast Text를 사용하여 핵심 키워드 추출
![fastText](./img/fastText.JPG)
##### 4) TF IDF를 사용하여 핵심 키워드 추출
![TFIDF](./img/TFIDF.JPG)
##### 5) 텐서 플로우 케라스를 활용하여 긍정/부정 기사 학습
![tensorFlow](./img/tensorFlow.JPG)